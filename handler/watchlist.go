package handler

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/AlienList/backend-watchlist/bob"
	"github.com/AlienList/backend-watchlist/db"
	"github.com/AlienList/backend-watchlist/models"
	"github.com/google/uuid"
	"github.com/streadway/amqp"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

type idKey string

var userIDKey = idKey("uId")
var watchlistIDkey = idKey("wlId")

func watchlists(router chi.Router) {
	router.Get("/", GetAllWatchList)
	router.Post("/", CreateWatchlist)

	router.Route("/{wlId}", func(router chi.Router) {
		router.Use(WatchlistContext)
		router.Get("/", GetWatchlist)
		router.Put("/", UpdateWatchlist)
		router.Delete("/", DeleteWatchlist)
	})

	router.Route("/user/{uId}", func(router chi.Router) {
		router.Use(UserContext)
		router.Get("/", GetWatchlistsByUserId)
	})

	router.Route("/schedule/{uId}", func(router chi.Router) {
		router.Use(UserContext)
		router.Get("/", GetAiringSchedulesByUserId)
	})
}

func health(router chi.Router) {
	router.Get("/", GetHealth)
}

func UserContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userIdStr := chi.URLParam(r, "uId")
		if userIdStr == "" {
			if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("user ID is required"))); err != nil {
				log.Println(err)
			}
			return
		}

		ctx := context.WithValue(r.Context(), userIDKey, userIdStr)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func WatchlistContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		watchlistIdStr := chi.URLParam(r, "wlId")
		if watchlistIdStr == "" {
			if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("watchlist ID is required"))); err != nil {
				log.Println(err)
			}
			return
		}

		ctx := context.WithValue(r.Context(), watchlistIDkey, watchlistIdStr)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func GetHealth(w http.ResponseWriter, r *http.Request) {
	render.Status(r, http.StatusOK)
	render.JSON(w, r, map[string]string{"message": "Service is healthy.", "service": "watchlist"})
}

func GetAllWatchList(w http.ResponseWriter, r *http.Request) {
	watchlists, err := dbInstance.GetAllWatchlists()
	if err != nil {
		if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
			log.Println(err)
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, watchlists)
}

func CreateWatchlist(w http.ResponseWriter, r *http.Request) {
	watchlist := &models.WatchRequest{}
	// here
	log.Println("creating watchlist")
	if err := render.Bind(r, watchlist); err != nil {
		if err := render.Render(w, r, ErrBadRequest); err != nil {
			// here
			log.Println("cant bind")
			log.Println(err)
		}
		return
	}
	if err := dbInstance.AddWatchlist(watchlist.Watchlist); err != nil {
		if err := render.Render(w, r, ErrorRenderer(err)); err != nil {
			log.Println(err)
		}
		return
	}

	render.Status(r, http.StatusCreated)
	render.JSON(w, r, watchlist)

	SendMessageToRabbitMQ(watchlist.Watchlist, true, watchlist.Genres)
}

func GetWatchlist(w http.ResponseWriter, r *http.Request) {
	watchlistIdStr := r.Context().Value(watchlistIDkey).(string)
	watchlistId, err := uuid.Parse(watchlistIdStr)
	if err != nil {
		if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("invalid watchlist ID"))); err != nil {
			log.Println(err)
		}
		return
	}

	user, err := dbInstance.GetWatchlistById(watchlistId)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, user)
}

func DeleteWatchlist(w http.ResponseWriter, r *http.Request) {
	watchlistIdStr := r.Context().Value(watchlistIDkey).(string)
	watchlistId, err := uuid.Parse(watchlistIdStr)
	if err != nil {
		if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("invalid watchlist ID"))); err != nil {
			log.Println(err)
		}
		return
	}
	if err != nil {
		if err := render.Render(w, r, ErrBadRequest); err != nil {
			log.Println(err)
		}
		return
	}

	err = dbInstance.DeleteWatchlist(watchlistId)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}
	render.DefaultResponder(w, r, "Successfully Deleted")
}

func UpdateWatchlist(w http.ResponseWriter, r *http.Request) {
	watchlistIdStr := r.Context().Value(watchlistIDkey).(string)
	watchlistId, err := uuid.Parse(watchlistIdStr)
	if err != nil {
		if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("invalid watchlist ID"))); err != nil {
			log.Println(err)
		}
		return
	}
	watchlist := &models.WatchRequest{}

	if err = render.Bind(r, watchlist); err != nil {
		if err := render.Render(w, r, ErrBadRequest); err != nil {
			log.Println(err)
		}
		return
	}
	retWatchlist, err := dbInstance.UpdateWatchlist(watchlistId, watchlist.Watchlist)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, retWatchlist)

	SendMessageToRabbitMQ(retWatchlist, false, watchlist.Genres)
}

func GetWatchlistsByUserId(w http.ResponseWriter, r *http.Request) {
	userIdStr := r.Context().Value(userIDKey).(string)
	userId, err := uuid.Parse(userIdStr)
	if err != nil {
		if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("invalid user ID"))); err != nil {
			log.Println(err)
		}
		return
	}

	user, err := dbInstance.GetWatchlistByUserId(userId)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, user)
}

func GetAiringSchedulesByUserId(w http.ResponseWriter, r *http.Request) {
	userIdStr := r.Context().Value(userIDKey).(string)
	userId, err := uuid.Parse(userIdStr)
	if err != nil {
		if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("invalid user ID"))); err != nil {
			log.Println(err)
		}
		return
	}

	user, err := dbInstance.GetAiringSchedulesByUserId(userId)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, user)
}

func RecommenderEngineScoreCalculator(watchlist *bob.Watchlist, isNewWatchlist bool) int {
	score := 0
	ratings := watchlist.Ratings.GetOrZero()
	if ratings != 0 {
		score += ratings - 3
	}
	if isNewWatchlist {
		score += 2
	}
	return score
}

func PrepareMessageForRabbitMQ(watchlist *bob.Watchlist, isNewWatchlist bool, genre string, buffer int) string {
	userId := watchlist.UserID
	animeId := watchlist.AnimeID
	score := RecommenderEngineScoreCalculator(watchlist, isNewWatchlist)
	category := genre
	dateTimeNow := time.Now().Add(time.Second * time.Duration(buffer)).Format("2006-01-02 15:04:05 -0700 -07")

	formattedString := fmt.Sprintf(
		`{
			"user_id": "%v",
			"anime_id": "%d",
			"category": "%s",
			"score": %d,
			"date_time": "%v"
		}`,
		userId, animeId, category, score, dateTimeNow)

	return formattedString
}

func SendMessageToRabbitMQ(watchlist *bob.Watchlist, isNewWatchlist bool, genres []string) {
	if len(genres) == 0 {
		log.Print("Genre is not passed, skipping write to rabbitmq")
		return
	}

	// Connect to RabbitMQ
	amqp_server := os.Getenv("AMQP_URL")
	conn, err := amqp.Dial(amqp_server)
	if err != nil {
		log.Printf("Failed to connect to RabbitMQ: %v", err)
	}
	defer func() {
		if err := conn.Close(); err != nil {
			log.Println(err)
		}
	}()
	// Create a channel
	ch, err := conn.Channel()
	if err != nil {
		log.Printf("Failed to open a channel: %v", err)
	}
	defer func() {
		if err := ch.Close(); err != nil {
			log.Println(err)
		}
	}()

	exchangeName := "recommenderUpdate.topic"
	queueName := "recommenderEngine"
	routingKey := "interactionDataPush.testing"

	// Declare the exchange (topic exchange)
	err = ch.ExchangeDeclare(
		exchangeName, // name
		"topic",      // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // no-wait
		nil,          // arguments
	)
	if err != nil {
		log.Printf("Failed to declare exchange: %v", err)
	}

	// Declare the queue
	_, err = ch.QueueDeclare(
		queueName, // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		log.Printf("Failed to declare queue: %v", err)
	}

	// Bind the queue to the exchange
	err = ch.QueueBind(
		queueName,    // queue name
		routingKey,   // routing key
		exchangeName, // exchange
		false,        // no-wait
		nil,          // arguments
	)
	if err != nil {
		log.Printf("Failed to bind queue to exchange: %v", err)
	}
	for index, genre := range genres {

		// Message to be published
		message := PrepareMessageForRabbitMQ(watchlist, isNewWatchlist, genre, index)

		// Publish the message to the exchange
		err = ch.Publish(
			exchangeName, // exchange
			routingKey,   // routing key
			false,        // mandatory
			false,        // immediate
			amqp.Publishing{
				DeliveryMode: 2, // persistent
				ContentType:  "application/json",
				Body:         []byte(message),
			},
		)
		if err != nil {
			log.Printf("Failed to publish a message: %v", err)
		} else {
			log.Println("Message published successfully")

			log.Printf("Message sent to RabbitMQ: %s", message)
		}
	}

}
