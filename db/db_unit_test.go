package db

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/AlienList/backend-watchlist/bob/factory"
	"github.com/aarondl/opt/null"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	log "github.com/sirupsen/logrus"
	"github.com/stephenafamo/bob"
	"gotest.tools/v3/assert"
)

var db_test Database

const (
	tableCreationQuery = `
		DROP TABLE IF EXISTS anime;
		CREATE TABLE IF NOT EXISTS anime (
			id serial4 NOT NULL,
			romaji varchar NULL,
			english varchar NULL,
			native varchar NULL,
			format varchar NULL,
			status varchar NULL,
			description varchar NULL,
			"startYear" int4 NULL,
			"startMonth" int4 NULL,
			"startDay" int4 NULL,
			"endYear" int4 NULL,
			"endMonth" int4 NULL,
			"endDay" int4 NULL,
			season varchar NULL,
			"seasonYear" varchar NULL,
			episodes varchar NULL,
			duration varchar NULL,
			"countryOfOrigin" varchar NULL,
			"updatedAt" int4 NULL,
			"coverImage" varchar NULL,
			"averageScore" int4 NULL,
			popularity int4 NULL,
			"AiringAt" int4 NULL,
			"nextAiringEpisode" int4 NULL,
			CONSTRAINT anime_pkey PRIMARY KEY (id)
		);

		DROP TABLE IF EXISTS users;
		CREATE EXTENSION IF NOT EXISTS pgcrypto;
		CREATE TABLE IF NOT EXISTS users 
		(
			user_id uuid not null default gen_random_uuid (),
			username character varying not null,
			passwordhash character varying not null,
			constraint users_pkey primary key (user_id),
			constraint users_username_key unique (username)
		) tablespace pg_default;

		DROP TABLE IF EXISTS watchlist;
		CREATE EXTENSION IF NOT EXISTS pgcrypto;
		CREATE TABLE IF NOT EXISTS watchlist (
			id uuid not null default gen_random_uuid (),
			anime_id integer not null,
			user_id uuid not null,
			ratings integer null,
			status character varying(255) null,
			airing_at integer null,
			next_airing_episode integer null,
			constraint watchlist_pkey primary key (id),
			constraint watchlist_anime_id_fkey foreign key (anime_id) references anime (id),
			constraint watchlist_user_id_fkey foreign key (user_id) references users (user_id)
		) tablespace pg_default;
	`
)

func setupTest(tb testing.TB) func(tb testing.TB) {
	insertWatchlists()
	// Return a function to teardown the test
	return func(tb testing.TB) {
		clearTable()
	}
}

func ensureTableExists() {
	if _, err := db_test.Conn.ExecContext(context.Background(), tableCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func clearTable() {
	if _, err := db_test.Conn.ExecContext(context.Background(), "DELETE FROM watchlist"); err != nil {
		log.Fatal(err)
	}
	if _, err := db_test.Conn.ExecContext(context.Background(), "DELETE FROM users"); err != nil {
		log.Fatal(err)
	}
	if _, err := db_test.Conn.ExecContext(context.Background(), "DELETE FROM anime"); err != nil {
		log.Fatal(err)
	}
}

func insertWatchlists() {
	f := factory.New()
	//anime templates
	animeTemplate1 := f.NewAnime(
		factory.AnimeMods.ID(21),
		factory.AnimeMods.English(null.From("ONE PIECE")),
		factory.AnimeMods.Status(null.From("RELEASING")),
		factory.AnimeMods.AiringAt(null.From(1697934600)),
		factory.AnimeMods.NextAiringEpisode(null.From(1080)),
	)
	animeTemplate2 := f.NewAnime(
		factory.AnimeMods.ID(384),
		factory.AnimeMods.English(null.From("GANTZ")),
		factory.AnimeMods.Status(null.From("FINISHED")),
	)

	//user templates
	uuid1, _ := uuid.Parse("c57d1af4-7620-4749-aeef-1ca76779f28b")
	uuid2, _ := uuid.Parse("804a790e-c908-499b-ab87-16e4a9d87fe0")
	uuid3, _ := uuid.Parse("6a70e3ae-c4f5-4b87-9c7e-d75aa9b9d37c")
	userTemplate1 := f.NewUser(factory.UserMods.UserID(uuid1), factory.UserMods.Username("Epith"), factory.UserMods.Passwordhash("$2a$14$hbIcJjvo7YkxmcSQw6QSJuNKc6VlrNSTVKgM7j/QsyzbV276uXbVi"))
	userTemplate2 := f.NewUser(factory.UserMods.UserID(uuid2), factory.UserMods.Username("Yum Yum Banana"))
	userTemplate3 := f.NewUser(factory.UserMods.UserID(uuid3), factory.UserMods.Username("Omurice"))
	//watchlist template
	wid1, _ := uuid.Parse("949dcd0f-d9c4-40a4-a88c-b0847fdfb440")
	wid2, _ := uuid.Parse("ccc81962-fe15-4a55-9b71-496b6d3dcb72")
	wid3, _ := uuid.Parse("815f00d7-e2d4-41dc-8c69-d76031c79008")
	watchlistTemplate1 := f.NewWatchlist(
		factory.WatchlistMods.ID(wid1),
		factory.WatchlistMods.UserID(uuid1),
		factory.WatchlistMods.AnimeID(21),
		factory.WatchlistMods.Status(null.From("watching")),
		factory.WatchlistMods.Ratings(null.From(3)),
		factory.WatchlistMods.AiringAt(null.From(1697934600)),
		factory.WatchlistMods.NextAiringEpisode(null.From(1080)),
		factory.WatchlistMods.WithAnime(animeTemplate1),
		factory.WatchlistMods.WithUser(userTemplate1),
	)
	watchlistTemplate2 := f.NewWatchlist(
		factory.WatchlistMods.ID(wid2),
		factory.WatchlistMods.UserID(uuid2),
		factory.WatchlistMods.AnimeID(384),
		factory.WatchlistMods.Status(null.From("watching")),
		factory.WatchlistMods.WithAnime(animeTemplate2),
		factory.WatchlistMods.WithUser(userTemplate2),
	)
	watchlistTemplate3 := f.NewWatchlist(
		factory.WatchlistMods.ID(wid3),
		factory.WatchlistMods.UserID(uuid3),
		factory.WatchlistMods.AnimeID(21),
		factory.WatchlistMods.Status(null.From("watching")),
		factory.WatchlistMods.AiringAt(null.From(1697934600)),
		factory.WatchlistMods.NextAiringEpisode(null.From(1080)),
		factory.WatchlistMods.WithUser(userTemplate3),
	)
	_, err := watchlistTemplate1.Create(context.Background(), db_test.Conn)
	if err != nil {
		log.Println(err)
	}
	_, err = watchlistTemplate2.Create(context.Background(), db_test.Conn)
	if err != nil {
		log.Println(err)
	}
	_, err = watchlistTemplate3.Create(context.Background(), db_test.Conn)
	if err != nil {
		log.Println(err)
	}
}

func TestMain(m *testing.M) {

	// uses a sensible default on windows (tcp/http) and linux/osx (socket)
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not construct pool: %s", err)
	}

	err = pool.Client.Ping()
	if err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	// pulls an image, creates a container based on it and runs it
	resource, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "11",
		Env: []string{
			"POSTGRES_PASSWORD=secret",
			"POSTGRES_USER=user_name",
			"POSTGRES_DB=dbname",
			"listen_addresses = '*'",
		},
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{Name: "no"}
	})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	port := resource.GetPort("5432/tcp")
	databaseUrl := fmt.Sprintf("postgres://user_name:secret@docker:%s/dbname?sslmode=disable", port)

	log.Println("Connecting to database on url: ", databaseUrl)

	err = resource.Expire(120) // Tell docker to hard kill the container in 120 seconds
	if err != nil {
		log.Println(err)
	}

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	pool.MaxWait = 120 * time.Second
	if err = pool.Retry(func() error {
		db_test.Conn, err = bob.Open("postgres", databaseUrl)
		if err != nil {
			return err
		}
		return db_test.Conn.PingContext(context.Background())
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	ensureTableExists()
	//Run tests
	code := m.Run()
	// You can't defer this because os.Exit doesn't care for defer
	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}

func TestGet_WatchlistsEmpty(t *testing.T) {
	watchlists, err := db_test.GetAllWatchlists()
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(watchlists), 0)
}

func TestGet_AllWatchlists(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	watchlists, err := db_test.GetAllWatchlists()
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(watchlists), 3)
	uuid1, _ := uuid.Parse("949dcd0f-d9c4-40a4-a88c-b0847fdfb440")
	uuid2, _ := uuid.Parse("ccc81962-fe15-4a55-9b71-496b6d3dcb72")
	uuid3, _ := uuid.Parse("815f00d7-e2d4-41dc-8c69-d76031c79008")
	assert.Equal(t, watchlists[0].ID, uuid1)
	assert.Equal(t, watchlists[1].ID, uuid2)
	assert.Equal(t, watchlists[2].ID, uuid3)
}

func TestGet_SingleWatchlist(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("949dcd0f-d9c4-40a4-a88c-b0847fdfb440")
	watchlist, err := db_test.GetWatchlistById(uuid)
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, watchlist.ID, uuid)
}

func TestGet_SingleInvalidWatchlistId(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("93a1fb47-0cba-4c74-8c17-30a42f75f3e9")
	_, err := db_test.GetWatchlistById(uuid)
	if err != nil {
		log.Println(err)
	}
	assert.ErrorContains(t, err, "no rows in result set")
}

func TestGet_WatchlistsByUserIdEmpty(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("123453ae-c4f5-4b87-9c7e-d75aa9b9d37c")
	watchlist, err := db_test.GetWatchlistByUserId(uuid)
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(watchlist), 0)
}
func TestGet_WatchlistsByUserIdNotEmpty(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("804a790e-c908-499b-ab87-16e4a9d87fe0")
	watchlists, err := db_test.GetWatchlistByUserId(uuid)
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(watchlists), 1)
	assert.Equal(t, watchlists[0].UserID, uuid)
}
func TestGet_WatchlistsByInvalidUserId(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("804a790e-c908-499b-ab87-16e4a9d87e")
	watchlists, err := db_test.GetWatchlistByUserId(uuid)
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(watchlists), 0)
}
func TestGet_SchedulesByUserIdNotEmpty(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("c57d1af4-7620-4749-aeef-1ca76779f28b")
	watchlists, err := db_test.GetAiringSchedulesByUserId(uuid)
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(watchlists), 1)
	assert.Equal(t, watchlists[0].UserID, uuid)
}
func TestGet_SchedulesByInvalidUserId(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("c57d1af4-7620-4749-aeef-1ca76779f28")
	watchlists, err := db_test.GetAiringSchedulesByUserId(uuid)
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(watchlists), 0)
}

func TestPost_NewValidWatchlist(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	f := factory.New()

	uuid1, _ := uuid.Parse("c57d1af4-7620-4749-aeef-1ca76779f28b")
	wid1, _ := uuid.Parse("8f6caaba-74d5-11ee-b962-0242ac120002")
	watchlistTemplate1 := f.NewWatchlist(
		factory.WatchlistMods.ID(wid1),
		factory.WatchlistMods.UserID(uuid1),
		factory.WatchlistMods.AnimeID(21),
		factory.WatchlistMods.Status(null.From("watching")),
	)
	err := db_test.AddWatchlist(watchlistTemplate1.Build())
	assert.NilError(t, err)
}

func TestDelete_ValidWatchlist(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("949dcd0f-d9c4-40a4-a88c-b0847fdfb440")
	err := db_test.DeleteWatchlist(uuid)
	assert.NilError(t, err)
}

func TestDelete_InvalidWatchlist(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("93a1fb47-0cba-4c74-8c17-30a42f75f3e9")
	err := db_test.DeleteWatchlist(uuid)
	assert.ErrorContains(t, err, "watchlist does not exist")
}
